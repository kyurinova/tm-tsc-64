package ru.tsc.kyurinova.tm.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.api.service.IAdminDataService;
import ru.tsc.kyurinova.tm.api.service.ILogService;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;
import ru.tsc.kyurinova.tm.api.service.dto.*;
import ru.tsc.kyurinova.tm.endpoint.*;
import ru.tsc.kyurinova.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Setter
@Getter
@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @NotNull
    @Autowired
    protected ILogService logService;

    @NotNull
    @Autowired
    protected ITaskDTOService taskService;

    @NotNull
    @Autowired
    protected IProjectDTOService projectService;

    @NotNull
    @Autowired
    protected IProjectTaskDTOService projectTaskService;

    @NotNull
    @Autowired
    protected ISessionDTOService sessionService;

    @NotNull
    @Autowired
    protected IAdminDataService adminDataService;

    @NotNull
    @Autowired
    protected IUserDTOService userService;

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    public void start(@Nullable final String[] args) {
        try {
            System.out.println("** WELCOME TO TASK MANAGER **");
            initPID();
            initEndpoint(endpoints);
            logService.info("** TASK-MANAGER SERVER STARTED **");
            Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        } catch (@NotNull final Exception e) {
            logService.error(e);
            System.exit(1);
        }
    }

    private void initEndpoint(@NotNull final AbstractEndpoint[] endpoints) {
        for (@NotNull final AbstractEndpoint endpoint : endpoints) {
            registry(endpoint);
        }
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        final String host = propertyService.getServerHost();
        final String port = propertyService.getServerPort();
        final String name = endpoint.getClass().getSimpleName();
        final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    private void prepareShutdown() {
        logService.info("** TASK-MANAGER SERVER STOPPED **");
    }

}
