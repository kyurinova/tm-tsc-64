package ru.tsc.kyurinova.tm.configuration;

import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.tsc.kyurinova.tm")
@EnableJpaRepositories("ru.tsc.kyurinova.tm.repository")
public class ServerConfiguration {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Bean
    @NotNull
    public DataSource dataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(propertyService.getJdbcDriver());
        dataSource.setUrl(propertyService.getJdbcUrl());
        dataSource.setUsername(propertyService.getJdbcUser());
        dataSource.setPassword(propertyService.getJdbcPassword());
        return dataSource;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(@NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(@NotNull final DataSource dataSource) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.tsc.kyurinova.tm");
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DIALECT, propertyService.getJdbcSqlDialect());
        properties.put(Environment.HBM2DDL_AUTO, propertyService.getJdbcNbm2ddlAuto());
        properties.put(Environment.SHOW_SQL, propertyService.getJdbcShowSql());
        properties.put(Environment.FORMAT_SQL, propertyService.getFormatSql());
        properties.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getSecondLevelCache());
        properties.put(Environment.CACHE_REGION_FACTORY, propertyService.getCacheRegionFactory());
        properties.put(Environment.USE_QUERY_CACHE, propertyService.getUseQueryCache());
        properties.put(Environment.USE_MINIMAL_PUTS, propertyService.getUseMinPuts());
        properties.put(Environment.CACHE_REGION_PREFIX, propertyService.getCacheRegionPrefix());
        properties.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getCacheProviderConfig());
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

}

