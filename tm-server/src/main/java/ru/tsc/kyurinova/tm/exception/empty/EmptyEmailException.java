package ru.tsc.kyurinova.tm.exception.empty;

import ru.tsc.kyurinova.tm.exception.AbstractException;

public class EmptyEmailException extends AbstractException {

    public EmptyEmailException() {
        super("Error. Email is empty.");
    }

    public EmptyEmailException(String value) {
        super("Error." + value + " Email is empty.");
    }

}
