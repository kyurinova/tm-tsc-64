package ru.tsc.kyurinova.tm.service.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kyurinova.tm.api.service.model.ITaskService;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.exception.empty.*;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.model.Task;
import ru.tsc.kyurinova.tm.model.User;
import ru.tsc.kyurinova.tm.repository.model.TaskRepository;
import ru.tsc.kyurinova.tm.repository.model.UserRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
public class TaskService extends AbstractOwnerService<Task> implements ITaskService {

    @NotNull
    @Autowired
    public TaskRepository taskRepository;

    @NotNull
    @Autowired
    public UserRepository userRepository;

    @Override
    @Transactional
    public void addAll(@NotNull final List<Task> tasks) {
        for (Task task : tasks) {
            taskRepository.save(task);
        }
    }

    @NotNull
    @Override
    @Transactional
    public Task create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Task task = new Task();
        User user = userRepository.findById(userId).orElse(null);
        task.setUser(user);
        task.setName(name);
        taskRepository.save(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public Task create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Task task = new Task();
        User user = userRepository.findById(userId).orElse(null);
        task.setUser(user);
        task.setName(name);
        task.setDescription(description);
        taskRepository.save(task);
        return task;
    }

    @Nullable
    @Override
    public Task findByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        taskRepository.deleteByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    public void updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @NotNull final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Task Task = findById(userId, id);
        Task.setName(name);
        Task.setDescription(description);
        taskRepository.save(Task);
    }

    @Override
    @Transactional
    public void updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @NotNull final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Task Task = findByIndex(userId, index);
        Task.setName(name);
        Task.setDescription(description);
        taskRepository.save(Task);
    }

    @Override
    @Transactional
    public void startById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        Task task = findById(userId, id);
        task.setStatus(Status.IN_PROGRESS);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void startByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        Task task = findByIndex(userId, index);
        task.setStatus(Status.IN_PROGRESS);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void startByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Task task = findByName(userId, name);
        task.setStatus(Status.IN_PROGRESS);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void finishById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        Task task = findById(userId, id);
        task.setStatus(Status.COMPLETED);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void finishByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        Task task = findByIndex(userId, index);
        task.setStatus(Status.COMPLETED);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void finishByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Task task = findByName(userId, name);
        task.setStatus(Status.COMPLETED);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        Task task = findById(userId, id);
        task.setStatus(status);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) throw new EmptyStatusException();
        Task task = findByIndex(userId, index);
        task.setStatus(status);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void changeStatusByName(@Nullable final String userId, @Nullable final String name, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        Task task = findByName(userId, name);
        task.setStatus(status);
        taskRepository.save(task);
    }

    @Nullable
    @Override
    public Task findByProjectAndTaskId(@Nullable final String userId, @Nullable String projectId, @Nullable String taskId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        return taskRepository.findByUserIdAndProjectIdAndId(userId, projectId, taskId);
    }

}
