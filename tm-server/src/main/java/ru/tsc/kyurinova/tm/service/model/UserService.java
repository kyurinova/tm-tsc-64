package ru.tsc.kyurinova.tm.service.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;
import ru.tsc.kyurinova.tm.api.service.model.IUserService;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.exception.empty.*;
import ru.tsc.kyurinova.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kyurinova.tm.exception.user.EmailExistsException;
import ru.tsc.kyurinova.tm.exception.user.LoginExistsException;
import ru.tsc.kyurinova.tm.model.User;
import ru.tsc.kyurinova.tm.repository.model.UserRepository;
import ru.tsc.kyurinova.tm.util.HashUtil;

import javax.transaction.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
public class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    @Autowired
    public UserRepository userRepository;

    @Autowired
    private IPropertyService propertyService;

    @Override
    @Transactional
    public void addAll(@NotNull final List<User> users) {
        for (User user : users) {
            userRepository.save(user);
        }
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable User user = userRepository.findByLogin(login);
        return user;
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable User user = userRepository.findByEmail(email);
        return user;
    }

    @Override
    public void isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final User user = userRepository.findByLogin(login);
        if (user != null) throw new LoginExistsException();
    }

    @Override
    public void isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final User user = userRepository.findByEmail(email);
        if (user != null) throw new EmailExistsException();
    }

    @Override
    @Transactional
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.deleteByLogin(login);
    }

    @NotNull
    @Override
    @Transactional
    public User createAdmin(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        isLoginExists(login);
        @NotNull final User user = new User();
        user.setRole(Role.ADMIN);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        userRepository.save(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        isLoginExists(login);
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        userRepository.save(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        isLoginExists(login);
        isEmailExists(email);
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        userRepository.save(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        isLoginExists(login);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        userRepository.save(user);
        return user;
    }

    @Nullable
    @Override
    @Transactional
    public User setPassword(@Nullable final String userId, @Nullable final String password) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = findById(userId);
        if (user == null) throw new EmptyUserIdException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        user.setPasswordHash(hash);
        userRepository.save(user);
        return user;
    }

    @Nullable
    @Override
    @Transactional
    public User updateUser(
            @Nullable final String userId,
            @NotNull final String firstName,
            @NotNull final String lastName,
            @NotNull final String middleName
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @Nullable final User user = findById(userId);
        if (user == null) return null;
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        userRepository.save(user);
        return user;
    }

    @Nullable
    @Override
    @Transactional
    public User lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty())
            throw new EmptyLoginException();
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) return null;
        user.setLocked("Y");
        userRepository.save(user);
        return user;
    }

    @Nullable
    @Override
    @Transactional
    public User unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty())
            throw new EmptyLoginException();
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) return null;
        user.setLocked("N");
        userRepository.save(user);
        return user;
    }

    @Override
    @Transactional
    public void remove(@Nullable final User entity) {
        if (entity == null) throw new EntityNotFoundException();
        userRepository.delete(entity);
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    @Transactional
    public void clear() {
        userRepository.deleteAll();
    }

    @Nullable
    @Override
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id).orElse(null);
    }

    @NotNull
    @Override
    public User findByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return userRepository.findAll().get(index);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        userRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @Nullable final User user = findByIndex(index);
        if (user != null) {
            userRepository.delete(user);
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return (userRepository.findById(id) != null);
    }

    @Override
    public boolean existsByIndex(@NotNull final Integer index) {
        return (findByIndex(index) != null);
    }

    @Override
    public int getSize() {
        return (int) userRepository.count();
    }

}
