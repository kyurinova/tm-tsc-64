package ru.tsc.kyurinova.tm.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.tsc.kyurinova.tm.endpoint.*;

@Configuration
@ComponentScan("ru.tsc.kyurinova.tm")
public class ClientConfiguration {

    @Bean
    @NotNull
    public AdminUserEndpoint adminUserEndpoint() {
        @NotNull final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();
        return adminUserEndpointService.getAdminUserEndpointPort();
    }

    @Bean
    @NotNull
    public AdminDataEndpoint adminDataEndpoint() {
        @NotNull final AdminDataEndpointService adminDataEndpointService = new AdminDataEndpointService();
        return adminDataEndpointService.getAdminDataEndpointPort();
    }

    @Bean
    @NotNull
    public ProjectEndpoint projectEndpoint() {
        @NotNull final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
        return projectEndpointService.getProjectEndpointPort();
    }

    @Bean
    @NotNull
    public ProjectTaskEndpoint projectTaskEndpoint() {
        @NotNull final ProjectTaskEndpointService projectTaskEndpointService = new ProjectTaskEndpointService();
        return projectTaskEndpointService.getProjectTaskEndpointPort();
    }

    @Bean
    @NotNull
    public SessionEndpoint sessionEndpoint() {
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        return sessionEndpointService.getSessionEndpointPort();
    }

    @Bean
    @NotNull
    public TaskEndpoint taskEndpoint() {
        @NotNull final TaskEndpointService taskEndpointService = new TaskEndpointService();
        return taskEndpointService.getTaskEndpointPort();
    }

    @Bean
    @NotNull
    public UserEndpoint userEndpoint() {
        @NotNull final UserEndpointService userEndpointService = new UserEndpointService();
        return userEndpointService.getUserEndpointPort();
    }

}


