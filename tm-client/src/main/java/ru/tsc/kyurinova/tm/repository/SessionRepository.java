package ru.tsc.kyurinova.tm.repository;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.kyurinova.tm.api.repository.ISessionRepository;
import ru.tsc.kyurinova.tm.endpoint.SessionDTO;

@Repository
public class SessionRepository implements ISessionRepository {

    @Nullable
    private SessionDTO session;

    @Override
    @Nullable
    public SessionDTO getSession() {
        return session;
    }

    @Override
    public void setSession(@Nullable final SessionDTO session) {
        this.session = session;
    }

}