package ru.tsc.kyurinova.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.listener.AbstractListener;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class FileScanner implements Runnable {

    public final static String PATH = "./";

    private static final int INTERVAL = 1;

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Collection<String> commands = new ArrayList<>();

    @NotNull
    @Autowired
    public Bootstrap bootstrap;

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    public void init() {
        for (@NotNull final AbstractListener listener : listeners) {
            commands.add(listener.command());
        }
        es.scheduleWithFixedDelay(this::run, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void run() {
        @NotNull final File file = new File(PATH);
        Arrays.stream(file.listFiles())
                .filter(o -> o.isFile() && commands.contains(o.getName()))
                .forEach(o -> {
                    @NotNull final String name = o.getName();
                    try {
                        bootstrap.runCommand(name);
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        o.delete();
                    }
                });
    }

}
