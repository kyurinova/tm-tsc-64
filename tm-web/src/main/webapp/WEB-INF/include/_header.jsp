<html>
<head>
    <title>TASK MANAGER</title>
</head>
<style>
    h1 {
        font-family: Trebuchet MS;
    }

    a {
        font-family: Trebuchet MS;
        color: darkblue;
    }

    select {
        font-family: Trebuchet MS;
        width: 200px;
    }

    input[type="text"] {
        font-family: Trebuchet MS;
        width: 200px;
    }

    input[type="date"] {
        font-family: Trebuchet MS;
        width: 200px;
    }

</style>

<body>
<table width="100%" height="100%" border="1" style="border-collapse: collapse">
    <tr>
        <td height="35" width="200" nowrap="nowrap" align="center">
            <b>TASK MANAGER</b>
        </td>
        <td width="100%" align="center">
            <a href="/projects">PROJECTS</a>
            |
            <a href="/tasks">TASKS</a>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="100%" valign="top" style="border-collapse: collapse">