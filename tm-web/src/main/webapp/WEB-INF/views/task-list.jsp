<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>

<h3>TASK LIST</h3>

<table>
    <tr style="background-color: #f7ccfe;">
        <th style="width: 20%;">ID</th>
        <th style="width: 10%;">NAME</th>
        <th style="width: 17%;">DESCRIPTION</th>
        <th style="width: 20%;">PROJECT ID</th>
        <th style="width: 10%;">STATUS</th>
        <th style="width: 6%;">CREATED</th>
        <th style="width: 6%;">DATE END</th>
        <th style="width: 5%;">EDIT</th>
        <th style="width: 6%;">DELETE</th>
    </tr>
    <c:forEach var="task" items="${tasks}">
        <tr>
            <td>
                <c:out value="${task.id}"/>
            </td>
            <td>
                <c:out value="${task.name}"/>
            </td>
            <td>
                <c:out value="${task.description}"/>
            </td>
            <td>
                <c:out value="${task.projectId}"/>
            </td>
            <td>
                <c:out value="${task.status.displayName}"/>
            </td>
            <td>
                <fmt:formatDate pattern="dd.MM.yyyy" value="${task.created}"/>
            </td>
            <td>
                <fmt:formatDate pattern="dd.MM.yyyy" value="${task.dateFinish}"/>
            </td>
            <td>
                <a href="/task/edit/${task.id}"/>EDIT</a>
            </td>
            <td>
                <a href="/task/delete/${task.id}"/>DELETE</a>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="/task/create" style="padding-top: 20px;">
    <button>CREATE TASK</button>
</form>

<jsp:include page="../include/_footer.jsp"/>


