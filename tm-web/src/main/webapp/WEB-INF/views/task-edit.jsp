<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>

<h3>TASK EDIT</h3>

<form:form action="/task/edit/${task.id}" method="POST" modelAttribute="task">
    <form:input type="hidden" path="id"/>
    <p>
    <div>NAME:</div>
    <div><form:input type="text" path="name"/></div>
    </p>
    <p>
    <div>DESCRIPTION:</div>
    <div><form:input type="text" path="description"/></div>
    </p>
    <p>
    <div>Status:</div>
    <div>
        <form:select path="status">
            <form:option value="${null}" label="-----"/>
            <form:options items="${statuses}" itemLabel="displayName"/>
        </form:select>
    </div>
    </p>
    <p>
    <div>CREATED:</div>
    <div><form:input type="date" path="created"/>
    </div>
    </p>
    <p>
    <div>DATE END:</div>
    <div><form:input type="date" path="dateFinish"/>
    </div>
    </p>
    <div>PROJECT:</div>
    <form:select path="projectId">
        <form:option value="${null}" label="-----"/>
        <form:options items="${projects}" itemLabel="name" itemValue="id"/>
    </form:select>
    </p>
    <button type="submit" style="padding-top: 20px;">SAVE TASK</button>
</form:form>

<jsp:include page="../include/_footer.jsp"/>
