<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>

<h3>PROJECT LIST</h3>

<table>
    <tr style="background-color: #f7ccfe;">
        <th style="width: 20%;">ID</th>
        <th style="width: 15%;">NAME</th>
        <th style="width: 20%;">DESCRIPTION</th>
        <th style="width: 10%;">STATUS</th>
        <th style="width: 10%;">CREATED</th>
        <th style="width: 10%;">DATE END</th>
        <th style="width: 5%;">EDIT</th>
        <th style="width: 10%;">DELETE</th>
    </tr>
    <c:forEach var="project" items="${projects}">
        <tr>
            <td>
                <c:out value="${project.id}"/>
            </td>
            <td>
                <c:out value="${project.name}"/>
            </td>
            <td>
                <c:out value="${project.description}"/>
            </td>
            <td>
                <c:out value="${project.status.displayName}"/>
            </td>
            <td>
                <fmt:formatDate pattern="dd.MM.yyyy" value="${project.created}"/>
            </td>
            <td>
                <fmt:formatDate pattern="dd.MM.yyyy" value="${project.dateFinish}"/>
            </td>
            <td>
                <a href="/project/edit/${project.id}"/>EDIT</a>
            </td>
            <td>
                <a href="/project/delete/${project.id}"/>DELETE</a>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="/project/create" style="padding-top: 20px;">
    <button>CREATE PROJECT</button>
</form>

<jsp:include page="../include/_footer.jsp"/>

