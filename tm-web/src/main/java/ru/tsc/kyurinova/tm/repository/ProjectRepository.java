package ru.tsc.kyurinova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.kyurinova.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public class ProjectRepository {

    private static final ProjectRepository INSTANCE = new ProjectRepository();

    @NotNull
    private Map<String, Project> projects = new LinkedHashMap<>();

    {
        add(new Project("First Project"));
        add(new Project("Second Project"));
        add(new Project("Third Project"));
    }

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    public void create() {
        add(new Project("New Project" + System.currentTimeMillis()));
    }

    public void add(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    public void save(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    @NotNull
    public Collection<Project> findAll() {
        return projects.values();
    }

    @Nullable
    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    public void removeById(@NotNull final String id) {
        projects.remove(id);
    }

}
