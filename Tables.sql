CREATE TABLE IF NOT EXISTS public.tm_project
(
    row_id character varying
(
    255
) COLLATE pg_catalog."default" NOT NULL,
    user_id character varying
(
    255
) COLLATE pg_catalog."default" NOT NULL,
    descr character varying
(
    255
) COLLATE pg_catalog."default" NOT NULL,
    status character varying
(
    255
) COLLATE pg_catalog."default" NOT NULL DEFAULT 'NOT_STARTED':: character varying,
    created timestamp without time zone NOT NULL,
    start_dt timestamp
                      without time zone,
    finish_dt timestamp
                      without time zone,
    name character varying
(
    255
) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT tm_project_pkey PRIMARY KEY
(
    row_id
),
    CONSTRAINT tm_project_fkey FOREIGN KEY
(
    user_id
)
    REFERENCES public.tm_user
(
    row_id
) MATCH SIMPLE
                      ON UPDATE NO ACTION
                      ON DELETE NO ACTION
    )
    TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tm_project
    OWNER to postgres;

COMMENT
ON TABLE public.tm_project
    IS 'Projects';

CREATE TABLE IF NOT EXISTS public.tm_task
(
    row_id character varying
(
    255
) COLLATE pg_catalog."default" NOT NULL,
    user_id character varying
(
    255
) COLLATE pg_catalog."default" NOT NULL,
    project_id character varying
(
    255
) COLLATE pg_catalog."default",
    status character varying
(
    255
) COLLATE pg_catalog."default" NOT NULL DEFAULT 'NOT_STARTED':: character varying,
    created timestamp without time zone NOT NULL,
    start_dt timestamp
                      without time zone,
    finish_dt timestamp
                      without time zone,
    name character varying
(
    255
) COLLATE pg_catalog."default" NOT NULL,
    descr character varying
(
    255
) COLLATE pg_catalog."default",
    CONSTRAINT tm_task_pkey PRIMARY KEY
(
    row_id
),
    CONSTRAINT tm_task_fkey1 FOREIGN KEY
(
    user_id
)
    REFERENCES public.tm_user
(
    row_id
) MATCH SIMPLE
                      ON UPDATE NO ACTION
                      ON DELETE NO ACTION,
    CONSTRAINT tm_task_fkey2 FOREIGN KEY
(
    project_id
)
    REFERENCES public.tm_project
(
    row_id
) MATCH FULL
                      ON UPDATE NO ACTION
                      ON DELETE NO ACTION
    )
    TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tm_task
    OWNER to postgres;

COMMENT
ON TABLE public.tm_task
    IS 'Tasks';

CREATE TABLE IF NOT EXISTS public.tm_session
(
    row_id character varying
(
    255
) COLLATE pg_catalog."default" NOT NULL,
    user_id character varying
(
    255
) COLLATE pg_catalog."default" NOT NULL,
    signature character varying
(
    255
) COLLATE pg_catalog."default",
    time_stamp bigint,
    CONSTRAINT tm_session_pkey PRIMARY KEY
(
    row_id
),
    CONSTRAINT tm_session_fkey FOREIGN KEY
(
    user_id
)
    REFERENCES public.tm_user
(
    row_id
) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    )
    TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tm_session
    OWNER to postgres;

COMMENT
ON TABLE public.tm_session
    IS 'Sessions';

CREATE TABLE IF NOT EXISTS public.tm_user
(
    row_id character varying
(
    255
) COLLATE pg_catalog."default" NOT NULL,
    login character varying
(
    255
) COLLATE pg_catalog."default" NOT NULL,
    password_hash character varying
(
    255
) COLLATE pg_catalog."default" NOT NULL,
    role character varying
(
    255
) COLLATE pg_catalog."default" NOT NULL,
    email character varying
(
    255
) COLLATE pg_catalog."default",
    fst_name character varying
(
    255
) COLLATE pg_catalog."default",
    last_name character varying
(
    255
) COLLATE pg_catalog."default",
    mid_name character varying
(
    255
) COLLATE pg_catalog."default",
    locked character varying
(
    100
) COLLATE pg_catalog."default",
    CONSTRAINT tm_user_pkey PRIMARY KEY
(
    row_id
)
    )
    TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tm_user
    OWNER to postgres;

COMMENT
ON TABLE public.tm_user
    IS 'Users';
	