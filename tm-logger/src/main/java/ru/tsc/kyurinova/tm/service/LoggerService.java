package ru.tsc.kyurinova.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.Map;

@Service
public class LoggerService {

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();
    @NotNull
    private final MongoClient mongoClient = new MongoClient("localhost", 27017);
    @NotNull
    private final MongoDatabase mongoDatabase = mongoClient.getDatabase("tm_log");

    @SneakyThrows
    public void log(@NotNull final String text) {
        @NotNull final Map<String, Object> event = objectMapper.readValue(text, LinkedHashMap.class);
        @NotNull final String collectionName = event.get("table").toString();
        if (mongoDatabase.getCollection(collectionName) == null)
            mongoDatabase.createCollection(collectionName);
        @NotNull final MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);
        collection.insertOne(new Document(event));
    }

}
